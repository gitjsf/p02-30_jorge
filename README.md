# Práctica de Entornos de Desarrollo

#### Jorge Sánchez - Flor Sánchez  y Elia Dotor Puente

~~~
Enlace al repositorio y los commits realizados en la práctica

https://bitbucket.org/gitjsf/p02-30_jorge/src/master/

~~~



# Ejecuto comando "git init" en la carpeta donde voy a hacer mi REPOSITRIO LOCAL
~~~
➜  gitEntornos git:(master) ✗ git init
Inicializado repositorio Git vacío en /home/jsfs/Documentos/gitEntornos/.git/



~~~
# Ejecuto comando "git status" en la carpeta para ver el estado (Staged, Uestaged...) de los archivos que contiene.
~~~
➜  gitEntornos git:(master) git status
En la rama master

No hay commits todavía

Archivos sin seguimiento:
  (usa "git add <archivo>..." para incluirlo a lo que se será confirmado)

	programacion_Modular/

no hay nada agregado al commit pero hay archivos sin seguimiento presentes (usa "git add" para hacerles seguimiento)



~~~
# Ejecuto comando ""ls" de Linux para ver el contenido de la carpeta
~~~
➜  gitEntornos git:(master) ✗ ls
programacion_Modular



~~~
# Ejecuto comando "git add" para añadir los archivos que quiera en este caso hago "git add *" para añadirlos todos
~~~
➜  gitEntornos git:(master) ✗ git add *
➜  gitEntornos git:(master) ✗ ls
programacion_Modular
➜  gitEntornos git:(master) ✗ git status
En la rama master

No hay commits todavía

Cambios a ser confirmados:
  (usa "git rm --cached <archivo>..." para sacar del área de stage)

	nuevo archivo:  programacion_Modular/Calculadora.java
	nuevo archivo:  programacion_Modular/CambioBase.java
	nuevo archivo:  programacion_Modular/Combinaciones.java
	nuevo archivo:  programacion_Modular/Factorial.java
	nuevo archivo:  programacion_Modular/FactorialRecursividad.java
	nuevo archivo:  programacion_Modular/Fecha.java
	nuevo archivo:  programacion_Modular/McD.java
	nuevo archivo:  programacion_Modular/Mcd.java
	nuevo archivo:  programacion_Modular/Potencia.java
	nuevo archivo:  programacion_Modular/calculadora.java
	nuevo archivo:  programacion_Modular/combinaciones.java
	nuevo archivo:  programacion_Modular/combinacionesElia.java
	nuevo archivo:  programacion_Modular/mcdPrueba.java
	nuevo archivo:  programacion_Modular/potencia.java
	nuevo archivo:  programacion_Modular/rectangulo.java
	nuevo archivo:  programacion_Modular/salario.java
	
	
	
~~~
# Ejecuto comando "git commit -m" para añadir el comentario de la modificación que he hecho en el archivo
~~~

➜  gitEntornos git:(master) ✗ git commit -m "Primera subida JAVA"
[master (commit-raíz) 2a1497e] Primera subida JAVA
 16 files changed, 893 insertions(+)
 create mode 100644 programacion_Modular/Calculadora.java
 create mode 100644 programacion_Modular/CambioBase.java
 create mode 100644 programacion_Modular/Combinaciones.java
 create mode 100644 programacion_Modular/Factorial.java
 create mode 100644 programacion_Modular/FactorialRecursividad.java
 create mode 100644 programacion_Modular/Fecha.java
 create mode 100644 programacion_Modular/McD.java
 create mode 100644 programacion_Modular/Mcd.java
 create mode 100644 programacion_Modular/Potencia.java
 create mode 100644 programacion_Modular/calculadora.java
 create mode 100644 programacion_Modular/combinaciones.java
 create mode 100644 programacion_Modular/combinacionesElia.java
 create mode 100644 programacion_Modular/mcdPrueba.java
 create mode 100644 programacion_Modular/potencia.java
 create mode 100644 programacion_Modular/rectangulo.java
 create mode 100644 programacion_Modular/salario.java
 
 
 
~~~
# Ejecuto comando "git status" para ver el estado (Staged, Uestaged...) de los archivos que contiene.
~~~
➜  gitEntornos git:(master) ✗ git status
En la rama master
Archivos sin seguimiento:
  (usa "git add <archivo>..." para incluirlo a lo que se será confirmado)

	README.md

no hay nada agregado al commit pero hay archivos sin seguimiento presentes (usa "git add" para hacerles seguimiento)



~~~
# Ejecuto comando "git add README.md" para añadir el archivo
~~~
➜  gitEntornos git:(master) ✗ git add README.md



~~~
# Ejecuto comando "git status" para ver el estado (Staged, Uestaged...) de los archivos que contiene.
~~~
➜  gitEntornos git:(master) ✗ git status
En la rama master
Cambios a ser confirmados:
  (usa "git reset HEAD <archivo>..." para sacar del área de stage)

	nuevo archivo:  README.md



~~~
# Ejecuto comando "git cmmit -m" par aañadir el comentario sobre la modificación que he hecho el archivo README.md
~~~
➜  gitEntornos git:(master) ✗ git commit -m "Añado archivo readme"
[master 81c6ee9] Añado archivo readme
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 README.md



~~~
# Ejecuto comando "git status" para ver el estado (Staged, Uestaged...) de los archivos que contiene.
~~~
➜  gitEntornos git:(master) git status
En la rama master
Cambios no rastreados para el commit:
  (usa "git add <archivo>..." para actualizar lo que será confirmado)
  (usa "git checkout -- <archivo>..." para descartar los cambios en el directorio de trabajo)

	modificado:     README.md

sin cambios agregados al commit (usa "git add" y/o "git commit -a")
➜  gitEntornos git:(master) ✗ git add README.md

➜  gitEntornos git:(master) ✗ git status 
En la rama master
Cambios a ser confirmados:
  (usa "git reset HEAD <archivo>..." para sacar del área de stage)

	modificado:     README.md
	
	
	
~~~
# Ejecuto comando "git remote add" para establecer origin como ppal
~~~

➜  gitEntornos git:(master) ✗ git remote add origin "https://bitbucket.org/gitjsf/p02-30_jorge/src"



~~~
# Ejecuto comando "git push" para subir los cambios al REPOSITORIO REMOTO
~~~
➜  gitEntornos git:(master) ✗ git push origin master
Username for 'https://bitbucket.org': gitjsf
Password for 'https://gitjsf@bitbucket.org': 
Contando objetos: 22, listo.
Delta compression using up to 4 threads.
Comprimiendo objetos: 100% (20/20), listo.
Escribiendo objetos: 100% (22/22), 8.04 KiB | 2.68 MiB/s, listo.
Total 22 (delta 4), reused 0 (delta 0)
To https://bitbucket.org/gitjsf/p02-30_jorge/src
 * [new branch]      master -> master
 
 
 
~~~
# Ejecuto comando "git status" para ver el estado (Staged, Uestaged...) de los archivos que contiene, 
# y ver que se creado el archivo ".gitignore"
~~~
➜  gitEntornos git:(master) ✗ git status
En la rama master
Cambios a ser confirmados:
  (usa "git reset HEAD <archivo>..." para sacar del área de stage)

	modificado:     README.md

Archivos sin seguimiento:
  (usa "git add <archivo>..." para incluirlo a lo que se será confirmado)

	.gitignore
	
~~~
# Ejecuto comando "git add" para añadir el archivo ".gitignore"
~~~

➜  gitEntornos git:(master) ✗ git add .gitignore


~~~
# Ejecuto comando "git status" para ver el estado (Staged, Uestaged...) de los archivos que contiene.
~~~
➜  gitEntornos git:(master) ✗ git status
En la rama master
Cambios a ser confirmados:
  (usa "git reset HEAD <archivo>..." para sacar del área de stage)

	nuevo archivo:  .gitignore
	modificado:     README.md
	
	

~~~
# Ejecuto comando "git push" para subir los cambios al REPOSITORIO REMOTO
~~~

➜  gitEntornos git:(master) ✗ git push origin master
Username for 'https://bitbucket.org': gitjsf
Password for 'https://gitjsf@bitbucket.org': 
Everything up-to-date
➜  gitEntornos git:(master) ✗ 
➜  gitEntornos git:(master) ✗ git status
En la rama master
Cambios a ser confirmados:
  (usa "git reset HEAD <archivo>..." para sacar del área de stage)

	nuevo archivo:  .gitignore
	modificado:     README.md

Cambios no rastreados para el commit:
  (usa "git add <archivo>..." para actualizar lo que será confirmado)
  (usa "git checkout -- <archivo>..." para descartar los cambios en el directorio de trabajo)

	modificado:     programacion_Modular/Fecha.java


➜  gitEntornos git:(master) ✗ git status
En la rama master
Cambios a ser confirmados:
  (usa "git reset HEAD <archivo>..." para sacar del área de stage)

	nuevo archivo:  .gitignore
	modificado:     README.md

Cambios no rastreados para el commit:
  (usa "git add <archivo>..." para actualizar lo que será confirmado)
  (usa "git checkout -- <archivo>..." para descartar los cambios en el directorio de trabajo)

	modificado:     programacion_Modular/Fecha.java

➜  gitEntornos git:(master) ✗ git add programacion_Modular/Fecha.java
➜  gitEntornos git:(master) ✗ git status
En la rama master
Cambios a ser confirmados:
  (usa "git reset HEAD <archivo>..." para sacar del área de stage)

	nuevo archivo:  .gitignore
	modificado:     README.md
	modificado:     programacion_Modular/Fecha.java

➜  gitEntornos git:(master) ✗ git commit -m "Version Jorge"
[master 5c778a4] Version Jorge
 3 files changed, 9 insertions(+)
 create mode 100644 .gitignore
~~~

 
**Elia**  
Clono el repositorio de **Jorge** en mi máquina con el comando `git clone`

~~~
alumno@profesor-B85M-DS3H-A:~/ELIA/Entornos$ git clone https://eliadotor@bitbucket.org/gitjsf/p02-30_jorge.git
Clonando en 'p02-30_jorge'...
remote: Counting objects: 39, done.
remote: Compressing objects: 100% (36/36), done.
remote: Total 39 (delta 11), reused 0 (delta 0)
Desempaquetando objetos: 100% (39/39), listo.
~~~

Entramos en nuestro repositorio

~~~      
alumno@profesor-B85M-DS3H-A:~/ELIA/Entornos$ cd p02-30_jorge
~~~

Creo una nueva rama llamada *desarrolloelia* donde llevaré a cabo mis modificaciones

~~~
alumno@profesor-B85M-DS3H-A:~/ELIA/Entornos/p02-30_jorge$ git branch desarrolloelia
~~~

Comprobamos que se ha creado la nueva rama con `git branch` viendo que ahora tenemos dos ramas:
~~~
alumno@profesor-B85M-DS3H-A:~/ELIA/Entornos/p02-30_jorge$ git branch
  desarrolloelia
* master
~~~

Cambiamos a la nueva rama con el comando `git checkout`
~~~
alumno@profesor-B85M-DS3H-A:~/ELIA/Entornos/p02-30_jorge$ git checkout desarrolloelia
Cambiado a rama 'desarrolloelia'
~~~

Hacemos un cambio en el fichero Fecha que es el mismo fichero que va a modificar **Jorge** y comprobamos el estado con `git status`:
~~~
alumno@profesor-B85M-DS3H-A:~/ELIA/Entornos/p02-30_jorge$ git status
En la rama desarrolloelia
Cambios no rastreados para el commit:
  (usa "git add <archivo>..." para actualizar lo que será confirmado)
  (usa "git checkout -- <archivo>..." para descartar los cambios en el directorio de trabajo)

	modificado:     programacion_Modular/Fecha.java

sin cambios agregados al commit (usa "git add" y/o "git commit -a")
~~~


Tras ver que tenemos el fichero *Fecha.java* sin seguimiento, realizamos un `git add`para añadir nuestros cambios y que esté ahora en seguimiento.

~~~
alumno@profesor-B85M-DS3H-A:~/ELIA/Entornos/p02-30_jorge$ git add .
~~~

Hacemos un `git status`vemos que ya está en seguimiento:

~~~
alumno@profesor-B85M-DS3H-A:~/ELIA/Entornos/p02-30_jorge$ git status
En la rama desarrolloelia
Cambios a ser confirmados:
  (usa "git reset HEAD <archivo>..." para sacar del área de stage)

	modificado:     programacion_Modular/Fecha.java
~~~

Posteriormente hacemos un `git commmit` con los cambios que hemos realizado

~~~
alumno@profesor-B85M-DS3H-A:~/ELIA/Entornos/p02-30_jorge$ git commit -m "Añado comentario en Fecha"
[desarrolloelia 256c0e9] Añado comentario en Fecha
 1 file changed, 1 insertion(+), 5 deletions(-)
~~~


Hago un `git merge` para fusionar mi rama con la rama master
~~~
alumno@profesor-B85M-DS3H-A:~/ELIA/Entornos/p02-30_jorge$ git merge desarrolloelia
Ya está actualizado.
~~~

Compruebo que sigo en la rama *desarrolloelia* con un `git branch`
~~~
alumno@profesor-B85M-DS3H-A:~/ELIA/Entornos/p02-30_jorge$ git branch
* desarrolloelia
  master
~~~

Y me cambio a la rama *master* con un `git checkout`
~~~
alumno@profesor-B85M-DS3H-A:~/ELIA/Entornos/p02-30_jorge$ git checkout master
Cambiado a rama 'master'
Tu rama está actualizada con 'origin/master'.
~~~

Desde la rama master hago un `git push` para subir mis cambios
~~~
alumno@profesor-B85M-DS3H-A:~/ELIA/Entornos/p02-30_jorge$ git push origin master
Password for 'https://eliadotor@bitbucket.org': 
Everything up-to-date
alumno@profesor-B85M-DS3H-A:~/ELIA/Entornos/p02-30_jorge$ 
~~~

 

# Ejecuto comando "git pull" para bajar a mi REPOSITORIO LOCAL los cambios de mi compañera Elia
~~~

➜  gitEntornos git:(master) git pull origin master
remote: Counting objects: 4, done.
remote: Compressing objects: 100% (4/4), done.
remote: Total 4 (delta 2), reused 0 (delta 0)
Desempaquetando objetos: 100% (4/4), listo.
Desde https://bitbucket.org/gitjsf/p02-30_jorge/src
 * branch            master     -> FETCH_HEAD
   81c6ee9..3c050cf  master     -> origin/master
Auto-fusionando programacion_Modular/Fecha.java
CONFLICTO (contenido): Conflicto de fusión en programacion_Modular/Fecha.java
Fusión automática falló; arregle los conflictos y luego realice un commit con el resultado.
➜  gitEntornos git:(master) ✗ 


~~~
# Ejecuto comando "git push" para subir los cambios al REPOSITORIO REMOTO, nos está COLISIONANDO
~~~
➜  gitEntornos git:(master) ✗ git push origin master
Username for 'https://bitbucket.org': gitjsf
Password for 'https://gitjsf@bitbucket.org': 
To https://bitbucket.org/gitjsf/p02-30_jorge/src
 ! [rejected]        master -> master (non-fast-forward)
error: fallo el push de algunas referencias a 'https://bitbucket.org/gitjsf/p02-30_jorge/src'
ayuda: Actualizaciones fueron rechazadas porque la punta de tu rama actual esta
ayuda: detrás de su contraparte remota. Integra los cambios remotos (es decir
ayuda: 'git pull ...') antes de hacer push de nuevo.
ayuda: Mira 'Note about fast-forwards' en 'git push --help' para mas detalles.



~~~
# Ejecuto comando "git push" para subir los cambios al REPOSITORIO REMOTO
~~~

➜  gitEntornos git:(master) ✗ git pull origin master
error: No es posible hacer pull porque tienes archivos sin fusionar.
ayuda: Corrígelos en el árbol de trabajo y entonces usa 'git add/rm <archivo>',
ayuda: como sea apropiado, para marcar la resolución y realizar un commit.
fatal: Saliendo porque existe un conflicto sin resolver.
➜  gitEntornos git:(master) ✗ git add Fecha.java
fatal: ruta especificada 'Fecha.java' no concordó con ninguna carpeta
➜  gitEntornos git:(master) ✗ git status
En la rama master
Tienes rutas no fusionadas.
  (arregla los conflictos y corre "git commit"
  (usa "git merge --abort" para abortar la fusion)

Rutas no fusionadas:
  (usa "git add <archivo>..." para marcar una resolución)

	ambos modificados:     programacion_Modular/Fecha.java

sin cambios agregados al commit (usa "git add" y/o "git commit -a")


~~~
# Ejecuto comando "git merge" para FUSIONAR los cambios, que cada uno hemos hecho, en el mismo archivo
~~~
➜  gitEntornos git:(master) ✗ git merge programacion_Modular/Fecha.java
error: No es posible hacer merge porque tienes archivos sin fusionar.
ayuda: Corrígelos en el árbol de trabajo y entonces usa 'git add/rm <archivo>',
ayuda: como sea apropiado, para marcar la resolución y realizar un commit.
fatal: Saliendo porque existe un conflicto sin resolver.
➜  gitEntornos git:(master) ✗ git add programacion_Modular/Fecha.java
➜  gitEntornos git:(master) ✗ git status
En la rama master
Todos los conflictos resueltos pero sigues fusionando.
  (usa "git commit" para concluir la fusión)

Cambios a ser confirmados:

	modificado:     programacion_Modular/Fecha.java


➜  gitEntornos git:(master) ✗ git commit -m "Cambios Elia y Jorge Fusionados"
[master a24ed91] Cambios Elia y Jorge Fusionados



~~~
# Ejecuto comando "git merge" y FUSIONA y deja todo actualizado
~~~

➜  gitEntornos git:(master) git merge master
Ya está actualizado.
➜  gitEntornos git:(master) 

~~~


