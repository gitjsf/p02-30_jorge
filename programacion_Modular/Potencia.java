package programacion_Modular;

import java.util.Scanner;

public class Potencia {

	public static void main(String[] args) 
	{
		int base = pedirBase();
		int exponente = pedirExponente();
		int potencia = calcularPotencia(base , exponente);
		verResultado (base , exponente , potencia);
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static int pedirBase()
	{
		Scanner entrada = new Scanner(System.in);
		System.out.println("Introduzca el número del que quiere calcular la potencia");
		return entrada.nextInt();
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static int pedirExponente()
	{
		Scanner entrada = new Scanner(System.in);
		System.out.println("Introduzca el exponente al que quiere elevar el numero");
		return entrada.nextInt();
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static int calcularPotencia(int base , int exponente)
	{	int potencia = 1;
		for ( int i = 1 ; i <= exponente; i++)
		{
			potencia = potencia * base;
		}
			return potencia;
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static void verResultado ( int base , int exponente , int potencia)
	{
		System.out.println("La potencia de: " + base + " elevado a " + exponente + " es: " + potencia);
	}
}
