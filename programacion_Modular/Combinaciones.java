package programacion_Modular;
import java.util.Scanner;
public class Combinaciones {
	public static void main(String[] args) 
	{
		int grupo = pedirGrupo();
		int factorialGrupo = calcularFactorialGrupo(grupo);
		int elementos = pedirElementos();
		int factorialElementos = calcularFactorialElementos(elementos);
		int factorialDif = (grupo - elementos);
		int factorialDiferencia = calcularFactorialDif(factorialDif);
		int combinaciones = calcularCombinaciones(factorialGrupo , factorialElementos , factorialDiferencia);
		verResultado (factorialGrupo , factorialElementos , factorialDif);
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////
	public static int pedirGrupo()
	{
		Scanner entrada = new Scanner(System.in);
		System.out.println("¿De cuantos elementos se compone el grupo?  --->");
		return entrada.nextInt();
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////
	public static int pedirElementos()
	{
		Scanner entrada = new Scanner(System.in);
		System.out.println("¿De cuantos elementos quiere hacer la combinacion?  --->");
		return entrada.nextInt();
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////
	public static int calcularFactorialElementos(int elementos)
	{
		if (elementos == 0)
			return 1;
		else 
		{
			return elementos * calcularFactorialElementos (elementos-1);
		}	
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////
	public static int calcularFactorialGrupo(int grupo)
	{
		if (grupo == 0)
			return 1;
		else 
		{
			return grupo * calcularFactorialGrupo (grupo-1);
		}	
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////
	public static int calcularFactorialDif(int factorialDiferencia)
	{
		if (factorialDiferencia == 0)
			return 1;
		else 
		{
			return factorialDiferencia * calcularFactorialDif (factorialDiferencia-1);
		}	
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////
	public static int calcularCombinaciones (int factorialGrupo , int factorialElementos , int factorialDif)
	{
		return factorialGrupo/factorialElementos*factorialDif;
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////
	public static void verResultado (int factorialGrupo , int factorialElementos , int factorialDif)
	{
		int combinaciones = ((factorialGrupo)/((factorialElementos)*(factorialDif)));
		System.out.println("El numero de combianciones posibles es: " + combinaciones );
	}
}
