package programacion_Modular;

import java.util.Scanner;

public class salario {
	
	static Scanner entrada = new Scanner(System.in);

	public static void main(String[] args) {
		int trabajadores = pedirTrabajadores();
		double tarifa = pedirTarifa();
		calcularSalario(trabajadores, tarifa);
	}

	public static int pedirTrabajadores() {
		System.out.println("Introduce el número de trabajadores");
		return entrada.nextInt();
		
	}
	
	public static double pedirTarifa() {
		System.out.println("Introduce la tarifa ordinaria");
		double tarifa = entrada.nextInt();
		boolean tarifaValida;
		do {
			tarifaValida = (tarifa > 0);
			if(!tarifaValida) {
				System.out.println("ERROR. La tarifa debe ser mayor que 0.");
			}
		}while(!tarifaValida);
		return tarifa;
		
	}
	
	public static int pedirHoras() {
		System.out.println("Introduce el número de horas");
		int horas = entrada.nextInt();
		return horas;
		
	}
	
	public static double calcularSalarioBruto(int horas, double tarifa){
		double bruto;
		int horasExtra = horas - 38;
		bruto = (tarifa*38) + ((horasExtra*1.5)*tarifa);
		return bruto;
		
		
	}
	public static double calcularImpuestos(int bruto){
		final int salarioMinimo = 600;
		double impuestos = 0;
		if(bruto <= 600) {
			impuestos = 0;
		}else {
			if(bruto <= 1200 ) {
				impuestos = (bruto - salarioMinimo)*0.25;
			}
			if(bruto <= 1800 ) {
				impuestos = (bruto - salarioMinimo)*0.45;
			}
		}
		return impuestos;
	}
	
	public static void verResultado(int bruto, int neto, int impuestos) {
		
	}
	
	public static void calcularSalario() {
		final int horasMinima = 38;
		
		verResultado();
	}
}
