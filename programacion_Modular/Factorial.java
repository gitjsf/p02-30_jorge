package programacion_Modular;

import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {
		
		int numero = pedirNumero();
		int factorial = calcularFactorial(numero);
		verResultado(numero, factorial);
		
	}
		public static int pedirNumero() {
			Scanner entrada = new Scanner(System.in);
			System.out.println("Introduzca un número para calcular su factorial");
			return entrada.nextInt();
		}
		
		public static int calcularFactorial(int numero) {
			int factorial = 1;
			for(int i=numero; i > 0; i--) {
				factorial = factorial * i;
			}
			return factorial;
			
		}
		
		public static void verResultado(int numero, int factorial) {
			System.out.println(numero + "! = " + factorial);

		}
		
		
}	
		
		
		
		
	


