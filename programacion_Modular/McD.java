package programacion_Modular;

import java.util.Scanner;

public class McD {

	public static void main(String[] args) 
	{
		int num1 = pedirNumero1();
		int num2 = pedirNumero2();
		int mcd = calcularMcd(num1, num2);
		verResultado (num1 , num2 , mcd);
	}
/////////////////////////////////////////////////////////////////////////////////////////////////
	public static int pedirNumero1()
	{
		Scanner entrada = new Scanner(System.in);
		System.out.println("Introduzca el número 1: ");
		return entrada.nextInt();
	}
/////////////////////////////////////////////////////////////////////////////////////////////////
	public static int pedirNumero2()
	{
		Scanner entrada = new Scanner(System.in);
		System.out.println("Introduzca el número 2: ");
		return entrada.nextInt();
	}
/////////////////////////////////////////////////////////////////////////////////////////////////
	public static int calcularMcd (int num1 , int num2)
	{
		while (num1 != num2)
			if (num1 > num2)
				num1 = num1 - num2;
			else
			{
				num2 = num2 - num1;
			}
		return 1;
	}
/////////////////////////////////////////////////////////////////////////////////////////////////
	public static void verResultado (int num1 , int num2 , int mcd)
	{
		System.out.println("El mcd es: " + mcd);
	}
}
