package programacion_Modular;

import java.util.Scanner;

public class potencia {
	public static void main(String args[]) {
		int base = pedirBase();
		int expo = pedirExponente();
		int calc = calcularPotencia(base, expo);
		verResultado (calc);
	}
	/////////////////////////////////////////////////////
	public static int pedirBase() {
		Scanner entrada = new Scanner (System.in);
		System.out.println("Introduce la base");
		return entrada.nextInt();
	}
	/////////////////////////////////////////////////////
	public static int pedirExponente() {
		Scanner entrada = new Scanner (System.in);
		System.out.println("Introduce el exponente");
		return entrada.nextInt();
	}
	////////////////////////////////////////////////////
	public static int calcularPotencia(int base, int expo) {
		int resultado = 1;
		for (int i = 0; i < expo; i++) {
			resultado = resultado * base;
		}
		return resultado;
	}
	public static void verResultado(int calc) {
		System.out.println("El resultado es: " + calc);
	}
}
