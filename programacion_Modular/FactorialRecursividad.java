package programacion_Modular;

import java.util.Scanner;

public class FactorialRecursividad {

	public static void main(String[] args) 
	///DECLARAMOS LAS VARIABLES QUE NECESITAREMOS
	{
		int numero = pedirNumero();
		int factorial = calcularFactorial(numero);
		verResultado (numero , factorial);
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////
	public static int pedirNumero()
	{
		Scanner entrada = new Scanner(System.in);
		System.out.println("Introduzca un número para calcular su facorial");
		return entrada.nextInt();
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////
	public static int calcularFactorial(int numero)
	{
		if (numero == 0)
			return 1;
		else 
		{
			return numero * calcularFactorial (numero-1);
		}
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////
	public static void verResultado (int numero , int factorial)
	{
		System.out.println("El factorial de " + numero + " es: " + factorial);
	}
}
